@extends('layouts.app')

@section('content')
	<h1>{{ $title }}</h1>
	<div class="chat">
		<div class="chat-window">
			<div class="output">

				<div class="container-chatbar">
					<img src="https://www.w3schools.com/w3images/bandmember.jpg" alt="Avatar">
					<p>Hello. How are you today?</p>
					<span class="time-right">11:00</span>
				</div>
				{{-- <div class="container-chatbar darker">
					<img src="https://www.w3schools.com/w3images/avatar_g2.jpg" alt="Avatar" class="right">
					<p>Hey! I'm fine. Thanks for asking!</p>
					<span class="time-left">11:01</span>
				</div> --}}
			</div>
		</div>
		<input type="hidden" id="handle" placeholder="Handle" />
		<div class="form-group">
			<input type="text" id="message" placeholder="Message" />
			<button id="send" class="btn btn-primary">Send</button>
		</div>
		
	</div>

@endsection
