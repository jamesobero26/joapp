@extends('layouts.app')

@section('content')
	<div class="jumbotron text-center">
		<h1>{{$title}}</h1>
		<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</p>

		@if (Auth::guest())
			<p>
				<a href="/login" class="btn btn-primary btn-lg" role="button">Login</a>
				<a href="/register" class="btn btn-success btn-lg" role="button">Register</a>
			</p>
		@endif
	</div>
@endsection